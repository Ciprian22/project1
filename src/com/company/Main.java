package com.company;

import java.io.IOException;

public class Main {

    // Iacob Ciprian

    public static void main(String[] args) throws IOException {
        String result = ReadWrite.readFile();
        ReadWrite.writeFile(result);
    }

}
