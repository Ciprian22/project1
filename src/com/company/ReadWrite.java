package com.company;

import java.io.*;
class ReadWrite {

    static String readFile() throws IOException {
        BufferedReader inputStream = new BufferedReader(new FileReader("test.txt"));
        String count;
        StringBuilder stringBuilder = new StringBuilder();
        while ((count = inputStream.readLine()) != null) {
            stringBuilder.append(count).append("\n");
        }
        inputStream.close();
        return stringBuilder.toString();
    }

    static void writeFile(String result) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter("test2.txt"));

        String[] parts = result.split("\n");
        for (String part : parts) {
            writer.write(part);
            writer.newLine();
        }
        writer.close();
    }
}